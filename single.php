<!--Página de post do site-->
<?php get_header();//adição do cabeçalho à página. header.php ?>
<div class="content">
	<aside class="container sidebar"><?php get_sidebar(); ?></aside>
	<div class="content_posts margin20">
		<?php if(have_posts()) : while( have_posts() ) : the_post();//loop de posts ?> 
			<section class="container">
				<h2>
					<a href="<?php the_permalink() ?>"><?php the_title()//título/link do post?></a>
				</h2> 
				<p>
					<?php the_date(); ?> | <?php the_time();//autor, data e hora do post?><br />
					<?php the_taxonomies();//exibe as categorias e taxonomias de cada post?>
				</p>
				<?php
					ob_start();//abertura do buffer
					the_content();//conteúdo armazenado no buffer
					$postContent = ob_get_contents();//captura dos dados do buffer
					ob_end_clean();//fechamendo do buffer
					echo $postContent;//adição do conteúdo do post na página
					wp_editor($postContent, 'editor', array('media_buttons' => false));//adição de editor com o conteúdo do post pré-carregado
				?>
				<p><?php comments_template();//adição do form de comentários?></p>
			</section>
		<?php endwhile; ?>
		<?php else: ?>
			<p>Não existem posts.</p>
		<?php endif ?>
	</div>
</div>
<?php get_footer();//adição do rodapé na página. footer.php ?>