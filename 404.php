<?php get_header(); ?>
	<div class="content">
		<article class="container container404">
			<header><h1>404 - Page Not Found</h1></header>
			<section><p>The article you were looking for was not found, but maybe try looking again!</p></section>
			<section><p><?php get_search_form(); ?></p></section>
		</article>
	</div>
<?php get_footer(); ?>