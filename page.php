<!--Template das páginas do wordpress-->
<?php get_header();//adição do cabeçalho à página. header.php ?>
<div class="content">
	<aside class="container sidebar"><?php get_sidebar(); ?></aside>
	<div class="content_posts margin20">
		<?php if(have_posts()) : while( have_posts() ) : the_post();//loop de posts ?> 
			<section class="container">
				<h2>
					<a href="<?php the_permalink() ?>"><?php the_title()//título/link do post?></a>
				</h2> 
				<p>
					<?php the_date(); ?> | <?php the_time();//autor, data e hora do post?><br />
					<?php the_taxonomies();//exibe as categorias e taxonomias de cada post?>
				</p>
				<?php the_content()//conteúdo do post?>
			</section>
		<?php endwhile; ?>
		<?php else: ?>
			<p>Não existem posts.</p>
		<?php endif ?>
	</div>
</div>
<?php get_footer();//adição do rodapé na página. footer.php ?>