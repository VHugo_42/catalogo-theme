
<!--Página de post do site-->
<?php get_header();//adição do cabeçalho à página. header.php ?>
<div class="content">	
	<aside class="container sidebar"><?php get_sidebar(); ?></aside><!--adição da barra lateral à esquerda do conteúdo-->
	
	<article class="post_list"><!--lista de posts de DVDs-->
		<header>DVDs</header>
		<?php
			$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
			// WP_Query arguments
			$args = array(
				//'category_name' => 'dvd',//lista por categoria dvd
				'post_type' => 'dvds',//lista por post type dvd
				'posts_per_page' => '5'
			);

			// The Query
			$queryDVD = new WP_Query($args);
			
			//if(have_posts()) : while(have_posts() ) : the_post();//loop de posts padrão
			if($queryDVD->have_posts()) : while($queryDVD->have_posts()) : $queryDVD->the_post();//loop de posts customizado
		?>

			<?php get_template_part('content');//content.php?>

		<?php endwhile; ?>	
		<?php else: ?>
			<p>Não existem posts.</p>
		<?php endif; ?>
	</article><!--fim da div post_list de DVDs-->

	<article class="post_list"><!--lista de posts de CDs-->
		<header>CDs</header>
		<?php
			// WP_Query arguments
			$args = array(
				//'category_name' => 'cd',//lista por categoria cd
				'post_type' => 'cds',//lista por post type cd
				'posts_per_page' => '5',
			);

			// The Query
			$queryCD = new WP_Query($args);
			
			//if(have_posts()) : while(have_posts() ) : the_post();//loop de posts padrão
			if($queryCD->have_posts()) : while($queryCD->have_posts()) : $queryCD->the_post();//loop de posts customizado
		?>

			<?php get_template_part('content');//content.php?>

		<?php endwhile; ?>	
		<?php else: ?>
			<p>Não existem posts.</p>
		<?php endif; ?>
	</article><!--fim da div post_list de CDs-->

	<article class="post_list"><!--lista de posts de Livros-->
		<header>Livros</header>
		<?php
			// WP_Query arguments
			$args = array(
				//'category_name' => 'livro',//lista por categoria cd
				'post_type' => 'livros',//lista por post type cd
				'posts_per_page' => '5',
			);

			// The Query
			$queryLivro = new WP_Query($args);
			
			//if(have_posts()) : while(have_posts() ) : the_post();//loop de posts padrão
			if($queryLivro->have_posts()) : while($queryLivro->have_posts()) : $queryLivro->the_post();//loop de posts customizado
		?>

			<?php get_template_part('content', 'livros');//content-livros.php?>

		<?php endwhile; ?>	
		<?php else: ?>
			<p>Não existem posts.</p>
		<?php endif; ?>
	</article><!--fim da div post_list de Livros-->
</div>
<?php get_footer();//adição do rodapé na página. footer.php ?>